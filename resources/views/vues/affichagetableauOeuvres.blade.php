@extends('layouts.master')
@section('content')

    <div class="container">
        <div class="blanc">
            <h1> Liste des Oeuvres</h1>
        </div>

        <table class="table table-bordered table-striped">
            <thead>
            <tr>
                <!--<th>id</th>-->
                <th>Titre</th>
                <th>Annee</th>
                <th>Type</th>
                <th>Artiste</th>
                <th>Illustration</th>
                <th><a class="btn_ajout" href="{{ url('/AjoutOeuvre') }}" role="button">+</a></th>
            </tr>
            </thead>
            @foreach($mesOeuvres as $uneO)
                <tr>
                    <!--<td>{{$uneO->OeuvreID}} </td>-->
                    <td>{{$uneO->Titre}} </td>
                    <td>{{$uneO->Annee}} </td>
                    <td>{{$uneO->Medium}} </td>
                    <td>{{$uneO->Nom}} </td>
                    <td><img class="photoO" src="{{$uneO->LienImage}}" ></td>
                    <td style="text-align: center">
                       <a href="{{url('/Oeuvres')}}/{{$uneO->OeuvreID}}">
                            <span class="glyphicon glyphicon-pencil"
                                  data-toggle="tooltip" data-placement="top" title="Modifier">

                            </span></a></td>
                    </td>
                </tr>
            @endforeach
            <BR><BR>
        </table>
    </div>
@stop
