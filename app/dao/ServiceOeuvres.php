<?php

namespace App\dao;

use App\Exceptions\MonException;
use Illuminate\Database\QueryException;
use Illuminate\Support\Facades\DB;

class ServiceOeuvres
{
    public function getListeOeuvre(){
        try {
            $mesOeuvres=DB::table('oeuvres')
                ->Select('OeuvreID','Titre','Annee','Medium','artistes.Nom','LienImage')
                ->join('artistes','oeuvres.ArtisteID','=','artistes.ArtisteID')
                ->get();
            return $mesOeuvres;
        }
        catch (QueryException $e){
            throw new MonException($e->getMessage(),5);
        }
    }

    public function ajoutOeuvre($titre,$annee,$medium,$idart,$image){
        try {
            DB::table('oeuvres')->insert(['Titre'=>$titre,'Annee'=>$annee,'Medium'=>$medium,'ArtisteID'=>$idart,'LienImage'=>"assets/images/".$image]);

        }
        catch (QueryException $e){
            throw new MonException($e->getMessage(),5);
        }
    }
}
