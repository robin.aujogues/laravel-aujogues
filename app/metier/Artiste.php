<?php

namespace App\metier;

class Artiste
{
    protected $table = 'artistes';
    protected $primaryKey='ArtisteID';

    protected $fillable=[
        'artisteID',
        'nom',
        'nationalite',
        'dateNaissance',
        'dateDeces'
    ];
    public $timestamps = false;
}
