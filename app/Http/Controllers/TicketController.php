<?php

namespace App\Http\Controllers;

use App\dao\ServiceArtistes;
use App\dao\ServiceOeuvres;
use App\dao\ServiceTickets;
use App\dao\ServiceVisiteur;
use App\Exceptions\MonException;
use Illuminate\Database\QueryException;
use Illuminate\Support\Facades\Request;

class TicketController
{
    public function listerTickets()
    {
        try{
            $unServiceTicket=new ServiceTickets();
            $mesTickets=$unServiceTicket->getListeTicket();
            return view('vues/affichagetableauTickets',compact('mesTickets'));
        }
        catch (QueryException $e){
            throw new MonException($e->getMessage(),5);
        }
    }


    public function ajoutTicket(){
        $unServiceV=new ServiceVisiteur();
        $mesV=$unServiceV->getListeVisiteur();

        return view('vues/formTicket',compact('mesV'));
    }

    public function postajouterTicket()
    {
        try {
            $nomID= Request::input('cbVisiteur');
            $date= Request::input('date');
            $prix = Request::input('prix');
            $unServiceT = new ServiceTickets();
            $unServiceT->ajoutTicket($nomID,$date,$prix);
            return view('/home');
        } catch (MonException $e) {
            $monErreur = $e->getMessage();
            return view('vues/pageErreur', compact('monErreur'));
        } catch (\Exception $ex) {
            $monErreur = $ex->getMessage();
            return view('vues/pageErreur', compact('monErreur'));
        }
    }
}
