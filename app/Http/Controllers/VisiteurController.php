<?php

namespace App\Http\Controllers;

use App\dao\ServiceArtistes;
use App\dao\ServiceOeuvres;
use App\dao\ServiceVisiteur;
use App\Exceptions\MonException;
use App\metier\Contact;
use App\metier\Visiteur;
use Illuminate\Database\QueryException;
use Illuminate\Support\Facades\Request;

class VisiteurController
{
    public function listerVisiteurs()
    {
        try{
            $unServiceVisiteurs=new ServiceVisiteur();
            $mesVisiteurs=$unServiceVisiteurs->getListeVisiteur();
            return view('vues/affichagetableauVisiteurs',compact('mesVisiteurs'));
        }
        catch (QueryException $e){
            throw new MonException($e->getMessage(),5);
        }
    }

    public function listerVisiteursT()
    {
        try{
            $unServiceVisiteurs=new ServiceVisiteur();
            $mesVisiteurs=$unServiceVisiteurs->getListeVisiteur();
            return view('vues/formTicket',compact('mesVisiteurs'));
        }
        catch (QueryException $e){
            throw new MonException($e->getMessage(),5);
        }
    }


    public function ajoutVisiteur(){
        $unServiceV=new ServiceVisiteur();
        $mesVisiteur=$unServiceV->getListeVisiteur();

        return view('vues/formVisiteur',compact('mesVisiteur'));
    }


    public function postajouterVisiteur()
    {
        try {
            $nom= Request::input('nom');
            $email= Request::input('email');
            $telephone = Request::input('telephone');
            $unServiceVisiteur = new ServiceVisiteur();
            $unServiceVisiteur->ajoutVisiteur($nom,$email,$telephone);
            return view('vues/formVisiteur');
        } catch (MonException $e) {
            $monErreur = $e->getMessage();
            return view('vues/pageErreur', compact('monErreur'));
        } catch (\Exception $ex) {
            $monErreur = $ex->getMessage();
            return view('vues/pageErreur', compact('monErreur'));
        }
    }

}
