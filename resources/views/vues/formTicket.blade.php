@extends('layouts.master')
@section('content')

    <div class="container">
        <div class="blanc">
            <h1>Ajouter un Ticket</h1>
        </div>
        {!! Form::open(['url'=>'ajoutTicket', 'files'=>true]) !!}
        <div class="col-md-12 well well-sm">
            <div class="form-horizontal">
                <div class="form-group">
                    <label class="col-md-3 control-label">Artiste : </label>
                    <div class="col-md-3">
                        <select name='cbVisiteur'>
                            @foreach($mesVisiteurs as $unV)
                                <option value={{$unV->VisiteurID}} >{{$unV->VisiteurID}}-{{$unV->Nom}}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
                <BR> <BR>
                <div class="form-group">
                    <label class="col-md-3 control-label">Date de visite : </label>
                    <div class="col-md-3">
                        <input id='date' type="date" name="date" value="{{Date('Y-m-d')}}" class="form-control" required autofocus>
                    </div>
                </div>
                <BR> <BR>
                <div class="form-group">
                    <label class="col-md-3 control-label">Prix en € : </label>
                    <div class="col-md-3">
                        <input type="text" id='prix' name="prix" value="15.0" class="form-control" readonly >
                    </div>
                </div>
                <BR> <BR>

                <div class="form-group">
                    <div class="col-md-6 col-md-offset-3">
                        <button type="submit" class="btn btn-default btn-primary"><span class="glyphicon glyphicon-ok"></span> Valider</button>
                        &nbsp;
                        <button type="button" class="btn btn-default btn-primary" onclick="{window.location = '{{url('/pageMenu')}}'}">
                            <span class="glyphicon glyphicon-remove"></span> Annuler
                        </button>
                    </div>
                </div>
            </div>
        </div>
        {!! Form::close() !!}
    </div>
@stop

