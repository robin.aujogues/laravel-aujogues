<?php

namespace App\dao;

use App\Exceptions\MonException;
use Illuminate\Database\QueryException;
use Illuminate\Support\Facades\DB;

class ServiceTickets
{
    public function getListeTicket(){
        try {
            $mesTickets=DB::table('tickets')
                ->Select('TicketID','visiteurs.Nom','DateVisite','Prix')
                ->join('visiteurs','tickets.VisiteurID','=','visiteurs.VisiteurID')
                ->get();
            return $mesTickets;
        }
        catch (QueryException $e){
            throw new MonException($e->getMessage(),5);
        }
    }

    public function ajoutTicket($visiteurId,$dateVisite,$prix){
        try {
            DB::table('tickets')->insert(['VisiteurID'=>$visiteurId,'DateVisite'=>$dateVisite,'Prix'=>$prix]);

        }
        catch (QueryException $e){
            throw new MonException($e->getMessage(),5);
        }
    }
}
