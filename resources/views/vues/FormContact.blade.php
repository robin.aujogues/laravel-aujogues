@extends('layouts.master')
@section('content')
    <div>
        <div class="container">
            <div class="blanc">
                <h1>Contactez-nous</h1>
            </div>
            {!! Form::open(['url' => 'ajoutContact', 'files' => true]) !!}
            <div class="col-md-12 well well-sm">
                <div class="form-group">
                    <label class="col-md-3 control-label">Nom : </label>
                    <div class="col-md-3">
                        <input id='nom' type="text" name="nom" value="" class="form-control" required autofocus>
                    </div>
                </div>
                <BR> <BR>
                <div class="form-group">
                    <label class="col-md-3 control-label">Email : </label>
                    <div class="col-md-3">
                        <input id='email' type="email" name="email" value="" class="form-control" required>
                    </div>
                </div>
                <BR> <BR>
                <div class="form-group">
                    <label class="col-md-3 control-label">Message : </label>
                    <div class="col-md-6">
                        <textarea id='message' name="message" class="form-control" rows="5" required></textarea>
                    </div>
                </div>
                <BR/> <BR/> <BR/>
                <div class="form-group">
                    <div class="col-md-6 col-md-offset-3">
                        <button type="submit" class="btn btn-default btn-primary"><span class="glyphicon glyphicon-ok"></span> Envoyer</button>
                        &nbsp;
                        <button type="button" class="btn btn-default btn-primary" onclick="window.location = '{{ url('/pageMenu') }}'">
                            <span class="glyphicon glyphicon-remove"></span> Annuler
                        </button>
                    </div>
                </div>
            </div>
            {!! Form::close() !!}
        </div>
    </div>
@endsection
