<?php

namespace App\dao;

use App\Exceptions\MonException;
use Illuminate\Database\QueryException;
use Illuminate\Support\Facades\DB;

class ServiceVisiteur
{
    public function getListeVisiteur(){
        try {
            $mesVisiteurs=DB::table('visiteurs')
                ->Select('VisiteurID','Nom','Email','Telephone')
                ->get();
            return $mesVisiteurs;
        }
        catch (QueryException $e){
            throw new MonException($e->getMessage(),5);
        }
    }

    public function getVisiteur($id){
        try{
            $unemploye=DB::table('visiteurs')
                ->select('VisiteurID','Nom','Email','Telephone')
                ->where('VisiteurID','=',$id)
                ->first();
            return $unemploye;
        }catch (\Illuminate\Database\QueryException $e){
            throw new MonException($e->getMessage(),5);
        }
    }

    public function getIdVisiteur(){
        try{
            return $this->getKey();

        }catch (QueryException $e){
            throw new MonException($e->getMessage(),5);
        }
    }

    public function ajoutVisiteur($nom,$email,$telephone){
        try {
            DB::table('visiteurs')->insert(['Nom'=>$nom,'Email'=>$email,'Telephone'=>$telephone]);

        }
        catch (QueryException $e){
            throw new MonException($e->getMessage(),5);
        }
    }
}
