<?php

namespace App\Http\Controllers;

use App\dao\ServiceArtistes;
use App\dao\ServiceManga;
use App\dao\ServiceOeuvres;
use App\Exceptions\MonException;
use Illuminate\Database\QueryException;
//use Illuminate\Http\Request;*
use Illuminate\Support\Facades\Request;
use mysql_xdevapi\Exception;


class OeuvreController extends Controller
{
    /**
     * @throws MonException
     */
    public function listerOeuvres()
    {
        try{
            $unServiceOeuvres=new ServiceOeuvres();
            $mesOeuvres=$unServiceOeuvres->getListeOeuvre();
            foreach ($mesOeuvres as $oeuvre){
                $chemin='assets\\images\\'.$oeuvre->LienImage;
                if(!file_exists($chemin)){
                    $oeuvre->couverture='assets\\images\\'.'erreur.png';
                }
                else
                    $oeuvre->couverture=$chemin;
            }
            return view('vues/affichagetableauOeuvres',compact('mesOeuvres'));
        }
        catch (QueryException $e){
            throw new MonException($e->getMessage(),5);
        }
    }

    public function ajoutOeuvre(){
        $unServiceArtiste=new ServiceArtistes();
        $mesArtistes=$unServiceArtiste->getListeArtistes();

        return view('vues/formOeuvreAjout',compact('mesArtistes'));
    }

    public function postajouterOeuvre()
    {
        try {
            $titre= Request::input('titre');
            $annee= Request::input('annee');
            $medium = Request::input('medium');
            $idart = Request::input('cbArtiste');
            $image = Request::input('image');
            $unServiceOeuvre = new ServiceOeuvres();
            $unServiceOeuvre->ajoutOeuvre($titre,$annee,$medium,$idart,$image);
            return view('/home');
        } catch (MonException $e) {
            $monErreur = $e->getMessage();
            return view('vues/pageErreur', compact('monErreur'));
        } catch (\Exception $ex) {
            $monErreur = $ex->getMessage();
            return view('vues/pageErreur', compact('monErreur'));
        }
    }
}
