<?php

namespace App\metier;

class Visiteur
{
    protected $table = 'visiteurs';
    protected $primaryKey='VisiteurID';

    protected $fillable=[
        'visiteurID',
        'nom',
        'email',
        'telephone'
    ];
    public $timestamps = false;
}
