<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\metier\Contact;

class ContactController extends Controller
{
    public function ajoutContact(Request $request)
    {
        $this->validate($request, [
            'nom' => 'required|string|max:255',
            'email' => 'required|email|max:255',
            'message' => 'required|string',
        ]);

        $contact = new Contact;
        $contact->nom = $request->nom;
        $contact->email = $request->email;
        $contact->message = $request->message;
        $contact->save();

        return redirect('/Contact');
    }
}
