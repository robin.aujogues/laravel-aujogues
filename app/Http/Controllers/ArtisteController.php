<?php

namespace App\Http\Controllers;

use App\dao\ServiceArtistes;
use App\dao\ServiceVisiteur;
use App\Exceptions\MonException;
use Illuminate\Database\QueryException;

class ArtisteController
{
    public function listerArtistes()
    {
        try{
            $unServiceArtiste=new ServiceArtistes();
            $mesArtistes=$unServiceArtiste->getListeArtistes();
            return view('vues/formOeuvreAjout',compact('mesArtistes'));
        }
        catch (QueryException $e){
            throw new MonException($e->getMessage(),5);
        }
    }
}
