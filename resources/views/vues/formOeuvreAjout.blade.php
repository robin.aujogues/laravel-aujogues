@extends('layouts.master')
@section('content')

<div class="container">
    <div class="blanc">
        <h1>Ajouter une Oeuvre</h1>
    </div>
    {!! Form::open(['url'=>'ajoutOeuvre', 'files'=>true]) !!}
    <div class="col-md-12 well well-sm">
        <div class="form-horizontal">
            <div class="form-group">
                <label class="col-md-3 control-label">Titre : </label>
                <div class="col-md-3">
                    <input id='titre' type="text" name="titre" value="" class="form-control" required autofocus>
                </div>
            </div>
            <BR> <BR>
            <div class="form-group">
                <label class="col-md-3 control-label">Année : </label>
                <div class="col-md-3">
                    <input id='annee' type="number" min="1900" max="2099" step="1" value="2000" name="annee" class="form-control">
                </div>
            </div>
            <div class="form-group">
                <label class="col-md-3 control-label">Médium : </label>
                <div class="col-md-3">
                    <input id='medium' type="text" value="Peinture" name="medium" class="form-control">
                </div>
            </div>
            <div class="form-group">
                <label class="col-md-3 control-label">Artiste : </label>
                <div class="col-md-3">
                 <select name='cbArtiste'>
                     @foreach($mesArtistes as $unA)
                        <option value={{$unA->ArtisteID}} >{{$unA->Nom}}</option>
                     @endforeach
                </select>
                </div>
            </div>
            <BR> <BR>
            <div class="form-group">
                <label class="col-md-3 control-label">Image: </label>
                <div class="col-md-3">
                    <input id='image' type="text" name="image" value="" class="form-control" required autofocus>
                </div>
            </div>
            <BR> <BR>
            <BR> <BR>

            <BR> <BR>

            <div class="form-group">
                <div class="col-md-6 col-md-offset-3">
                    <button type="submit" class="btn btn-default btn-primary"><span class="glyphicon glyphicon-ok"></span> Valider</button>
                    &nbsp;
                    <button type="button" class="btn btn-default btn-primary" onclick="{window.location = '{{url('/pageMenu')}}'}">
                        <span class="glyphicon glyphicon-remove"></span> Annuler
                    </button>
                </div>
            </div>
        </div>
    </div>
    {!! Form::close() !!}
</div>
@stop
