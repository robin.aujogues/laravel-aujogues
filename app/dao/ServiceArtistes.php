<?php

namespace App\dao;

use App\Exceptions\MonException;
use Illuminate\Database\QueryException;
use Illuminate\Support\Facades\DB;

class ServiceArtistes
{


    public function getListeArtistes(){
        try {
            $mesArtistes=DB::table('artistes')
                ->Select('ArtisteID','Nom','Nationalite','DateNaissance','DateDeces')
                ->get();

            return $mesArtistes;
        }
        catch (QueryException $e){
            throw new MonException($e->getMessage(),5);
        }
    }


}
