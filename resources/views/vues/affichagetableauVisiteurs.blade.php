@extends('layouts.master')
@section('content')

    <div class="container">
        <div class="blanc">
            <h1> Liste des Visiteurs</h1>
        </div>
        <table class="table table-bordered table-striped">
            <thead>
            <tr>
                <th>Prénom / Nom</th>
                <th>Email</th>
                <th>Téléphone</th>
                <th><a class="btn_ajout" href="{{ url('/AjoutVisiteur') }}" role="button">+</a></th>
            </tr>
            </thead>
            @foreach($mesVisiteurs as $unV)
                <tr>
                    <td>{{$unV->Nom}} </td>
                    <td>{{$unV->Email}} </td>
                    <td>{{$unV->Telephone}} </td>
                    <td style="text-align: center">
                        <a href="{{url('/Visiteurs')}}/{{$unV->VisiteurID}}">
                            <span class="glyphicon glyphicon-pencil"
                                  data-toggle="tooltip" data-placement="top" title="Modifier">

                            </span></a></td>
                    </td>
                </tr>
            @endforeach
            <BR><BR>
        </table>
    </div>
@stop

