<?php

namespace App\metier;

class Tickets
{
    protected $table = 'tickets';
    protected $primaryKey='TicketID';

    protected $fillable=[
        'TicketID',
        'VisiteurID',
        'DateVisite',
        'Prix'
    ];
    public $timestamps = false;
}
