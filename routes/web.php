<?php

use App\Http\Controllers\MangaController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/

Route::get('/', function () {
    return view('home');
});

Route::get('/pageMenu', function () {
    return view('home');
});

Route::get('/Oeuvres',[\App\Http\Controllers\OeuvreController::class,'listerOeuvres']);

Route::get('/Visiteurs',[\App\Http\Controllers\VisiteurController::class,'listerVisiteurs']);

Route::get('/Tickets',[\App\Http\Controllers\TicketController::class,'listerTickets']);

Route::get('/AjoutOeuvre',[\App\Http\Controllers\ArtisteController::class,'listerArtistes']);

Route::get('/AjoutTicket',[\App\Http\Controllers\VisiteurController::class,'listerVisiteursT']);

Route::get('/Contact',function (){
    return view('vues/FormContact');
});



Route::get('/AjoutVisiteur',function (){
    return view('vues/formVisiteur');
});

Route::post('/ajoutOeuvre',[\App\Http\Controllers\OeuvreController::class,'postajouterOeuvre']);

Route::post('/ajoutContact', [\App\Http\Controllers\ContactController::class, 'ajoutContact']);

Route::post('/ajoutVisiteur', [\App\Http\Controllers\VisiteurController::class, 'postajouterVisiteur']);

Route::post('/ajoutTicket', [\App\Http\Controllers\TicketController::class, 'postajouterTicket']);



