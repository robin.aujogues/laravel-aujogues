<?php

namespace App\metier;

use Illuminate\Database\Eloquent\Model;

class Contact extends Model
{
    protected $table = 'contact';
    protected $primaryKey='ContactID';

    protected $fillable=[
        'ContactID',
        'Nom',
        'Email',
        'Message',
    ];
    public $timestamps = false;
}
