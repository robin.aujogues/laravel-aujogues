<?php

namespace App\metier;

use Illuminate\Database\Eloquent\Model;
class Oeuvre extends Model
{
    protected $table = 'oeuvres';
    protected $primaryKey='OeuvreID';

    protected $fillable=[
        'OeuvreID',
        'Titre',
        'Annee',
        'Medium',
        'ArtisteID',
        'LienImage'
    ];
    public $timestamps = false;
}
