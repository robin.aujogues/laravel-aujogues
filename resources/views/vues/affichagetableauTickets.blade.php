@extends('layouts.master')
@section('content')

<div class="container">
    <div class="blanc">
        <h1> Liste des Tickets</h1>
    </div>
    <table class="table table-bordered table-striped">
        <thead>
        <tr>
            <th>Ticket n°</th>
            <th>Nom du visiteur</th>
            <th>Date de visite</th>
            <th>Prix</th>
            <th><a class="btn_ajout" href="{{ url('/AjoutTicket') }}" role="button">+</a></th>
        </tr>
        </thead>
        @foreach($mesTickets as $unT)
        <tr>
            <td>{{$unT->TicketID}} </td>
            <td>{{$unT->Nom}} </td>
            <td>{{$unT->DateVisite}} </td>
            <td>{{$unT->Prix}} €</td>
            <td style="text-align: center">
                <a href="{{url('/Tickets')}}/{{$unT->TicketID}}">
                            <span class="glyphicon glyphicon-pencil"
                                  data-toggle="tooltip" data-placement="top" title="Modifier">

                            </span></a></td>
            </td>
        </tr>
        @endforeach
        <BR><BR>
    </table>
</div>
@stop

