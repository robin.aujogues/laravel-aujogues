
@extends('layouts.master')

@section('content')
<div class="gallery">
    <div class="cont_title">
    <h1 class="main_title"><b>Bienvenue sur Mon Musée</b></h1>
    </div>

    <br/>
    <p class="intro"><b>Mon Musée est une collection d'œuvres d'art célèbres de certains des plus grands artistes de l'histoire. Explorez notre galerie pour découvrir des peintures emblématiques de Picasso, Monet, Van Gogh. Que vous soyez un amateur d'art passionné ou simplement curieux, nous espérons que vous apprécierez votre visite!</b></p>
    <div class="artist-1">
        <h2><b>Picasso</b></h2>
        <img src="assets/images/picasso.jpg" alt="Art de Picasso" style="max-height:250px;  float: left; margin-right: 15px;">
        <p><b>Pablo Picasso</b>, un des plus grands et plus influents artistes du 20ème siècle, est surtout connu pour avoir co-fondé le mouvement cubiste. Son travail se caractérise par son approche innovante de la forme, de la couleur et de la perspective. Picasso a eu une influence immense sur l'art moderne et continue d'inspirer les artistes du monde entier.</p>
    </div>

    <div class="artist">
        <h2><b>Monet</b></h2>
        <img src="assets/images/monet.jpg" alt="Art de Monet" style="max-height:250px;  float: right; margin-right: 15px;">
        <p ><b>Claude Monet </b>est l'un des fondateurs de l'impressionnisme, un mouvement qui cherchait à capturer la réalité de la lumière naturelle et les couleurs dans la peinture. Monet est célèbre pour son rôle dans l'innovation de la peinture en plein air, une caractéristique clé de l'impressionnisme. Ses œuvres continuent d'être admirées pour leur beauté et leur originalité.</p>
    </div>

    <div class="artist">
        <h2><b>Van Gogh</b></h2>
        <img src="assets/images/van_gogh.jpg" alt="Art de Van Gogh" style="max-height:250px;  float: left; margin-right: 15px;">
        <p><b>Vincent Van Gogh</b>, l'un des peintres les plus célèbres de l'histoire de l'art occidental, est connu pour son utilisation audacieuse de la couleur et son style de peinture expressif. Malgré une vie marquée par la maladie mentale, Van Gogh a produit une œuvre d'art incroyablement émouvante et vibrante qui continue d'inspirer les artistes et les amateurs d'art aujourd'hui.</p>
    </div>
</div>
</div>
@stop